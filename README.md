# NVDAMN


A replacement for the battery backed NVRAM + RTC components that are becoming difficult to source due to End Of Life.  This project is specifically aimed at allowing vintage computing equipment (Such as the Sun SPARC computers) to be preserved effectively.  In light of this, the design provides a replaceable battery unit and a mechanism to set and restore images of the original RAM state in order to allow rapid deployment without additional configuration steps OR expensive programmers.