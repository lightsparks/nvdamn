
# NVDAMN app.
Java application that allows users to upload and download configurations to the NVDAMN target.  This provides a means to ensure that configurations can be permanently stored for preservation.  It also provides a cross platform method of allowing new configurations to be transferred between users.
